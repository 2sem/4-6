﻿using System.Collections.Generic;
using Classes;
using L3.writers;

namespace L3.serializer
{
    interface ISerializer
    {
        void Serialize(List<Vehicle> vehicles, IStream stream);
        List<Vehicle> Deserialize(IStream stream);
    }
}
