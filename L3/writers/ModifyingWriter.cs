﻿using System.Collections.Generic;
using System.IO;
using Classes.Plugins;

namespace L3.writers
{
    public class ModifyingWriter : IStream
    {
        private string fileName;
        private List<ICipheringPlugin> plugins;

        public ModifyingWriter(string fileName, List<ICipheringPlugin> plugins)
        {
            this.fileName = fileName;
            this.plugins = plugins;
        }

        StreamReader IStream.OutputStream => new StreamReader(new ModifyingFileStream(fileName, FileMode.Open, plugins));

        StreamWriter IStream.InputStream => new StreamWriter(new ModifyingFileStream(fileName, FileMode.OpenOrCreate, plugins));
    }
}
