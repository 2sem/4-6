﻿using System.Collections.Generic;
using System.IO;
using Classes.Plugins;

namespace L3.writers
{
    class ModifyingFileStream : FileStream
    {
        private List<ICipheringPlugin> plugins;
        public ModifyingFileStream(string path, FileMode mode, List<ICipheringPlugin> plugins) : base(path, mode)
        {
            this.plugins = plugins;
        }

        public override int Read(byte[] array, int offset, int count)
        {
            int readCount = base.Read(array, offset, count);
            for (int i = 0; i < plugins.Count; i++)
            {
                if (plugins[i].Enabled)
                {
                    plugins[i].Decipher(array, offset, readCount);
                }
            }
            return readCount;
        }

        public override void Write(byte[] array, int offset, int count)
        {
            for (int i = plugins.Count - 1; i >= 0; i--)
            {
                if (plugins[i].Enabled)
                {
                    plugins[i].Encipher(array, offset, count);
                }
            }
            base.Write(array, offset, count);
        }
    }
}
