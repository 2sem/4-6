﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows;
using Classes;
using Classes.Plugins;
using L3.serializer;
using L3.writers;
using Microsoft.Win32;

namespace L3
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static readonly string PluginsDirectory = Path.Combine(Directory.GetCurrentDirectory(), "Plugins");
        private List<IFunctionPlugin> plugins = new List<IFunctionPlugin>();
        private List<ICipheringPlugin> cipheringPlugins;
        private ObjectsManager objectsManager;

        
        
        public MainWindow()
        {
            InitializeComponent();
            Loaded += MainWindow_Loaded;
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            objectsManager = ObjectsManager.Instance();
            lbUsers.ItemsSource = objectsManager.Passengers;
            btnAddUser.Click += BtnAddUser_Click;
            btnEditUser.Click += BtnEditUser_Click;
            lbCargos.ItemsSource = objectsManager.Cargos;
            btnAddCargo.Click += BtnAddCargo_Click;
            btnEditCargo.Click += BtnEditCargo_Click;
            lbSources.ItemsSource = objectsManager.EnergySources;
            btnAddSource.Click += BtnAddSource_Click;
            btnEditSource.Click += BtnEditSource_Click;
            lbEngines.ItemsSource = objectsManager.Engines;
            btnAddEngine.Click += BtnAddEngine_Click;
            btnEditEngine.Click += BtnEditEngine_Click;
            lbVehicles.ItemsSource = objectsManager.Vehicles;
            btnAddVehicle.Click += BtnAddVehicle_Click;
            btnEditVehicle.Click += BtnEditVehicle_Click;
            btnSave.Click += BtnSave_Click;
            btnLoad.Click += BtnLoad_Click;
            plugins = PluginLoader.LoadPlugins<IFunctionPlugin>(PluginsDirectory);
            cipheringPlugins = PluginLoader.LoadPlugins<ICipheringPlugin>(PluginsDirectory);


        }
        private void BtnEditVehicle_Click(object sender, RoutedEventArgs e)
        {
            if (lbVehicles.SelectedItem != null)
            {
                var vehicle = lbVehicles.SelectedItem as Vehicle;
                var editor = new AddVehicle(new List<Passenger>(objectsManager.Passengers),
                        new List<Cargo>(objectsManager.Cargos), new List<Engine>(objectsManager.Engines), vehicle);
                var addWindow = new AddWindow(editor);
                addWindow.ShowDialog();
            }
        }

        private void BtnLoad_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog
            {
                Filter = "Текстовый документ|*.txt"
            };
            if (fileDialog.ShowDialog().Value)
            {
                var fileName = fileDialog.FileName;
                LoadItems(fileName);
            }
        }

        private void LoadItems(string fileName)
        {
            var fileWriter = new ModifyingWriter(fileName, cipheringPlugins);
            var serializer = new XmlSerializer();
            List<Vehicle> Vehicles = serializer.Deserialize(fileWriter);
            objectsManager.Vehicles.Clear();
            objectsManager.Passengers.Clear();
            objectsManager.Cargos.Clear();
            objectsManager.EnergySources.Clear();
            objectsManager.Engines.Clear();
            foreach (Vehicle vehicle in Vehicles)
            {
                objectsManager.Vehicles.Add(vehicle);
                objectsManager.Engines.Add(vehicle.Engine);
                objectsManager.EnergySources.Add(vehicle.Engine.EnergySource);
                if (vehicle.GetType() == typeof(Car))
                {
                    foreach (Passenger passenger in ((Car)vehicle).Passengers)
                    {
                        objectsManager.Passengers.Add(passenger);
                    }
                }
                else if (vehicle.GetType() == typeof(Truck))
                {
                    foreach (Cargo cargo in ((Truck)vehicle).Cargos)
                    {
                        objectsManager.Cargos.Add(cargo);
                    }
                }
            }
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog fileDialog = new SaveFileDialog
            {
                Filter = "Текстовый документ|*.txt"
            };
            if (fileDialog.ShowDialog().Value)
            {
                var fileWriter = new ModifyingWriter(fileDialog.FileName, cipheringPlugins);
                var serializer = new XmlSerializer();
                serializer.Serialize(new List<Vehicle>(objectsManager.Vehicles), fileWriter);
            }
        }

        private void BtnEditEngine_Click(object sender, RoutedEventArgs e)
        {
            if (lbEngines.SelectedItem != null)
            {
                var engine = lbEngines.SelectedItem as Engine;
                var editor = new AddEngine(new List<EnergySource>(objectsManager.EnergySources), ref engine);
                var addWindow = new AddWindow(editor);
                addWindow.ShowDialog();
            }
        }

        private void BtnEditSource_Click(object sender, RoutedEventArgs e)
        {
            if (lbSources.SelectedItem != null)
            {
                var source = lbSources.SelectedItem as EnergySource;
                var editor = new AddSource() { DataContext = source };
                var addWindow = new AddWindow(editor);
                addWindow.ShowDialog();
            }
        }

        private void BtnEditCargo_Click(object sender, RoutedEventArgs e)
        {
            if (lbCargos.SelectedItem != null)
            {
                var cargo = lbCargos.SelectedItem as Cargo;
                var editor = new AddCargo() { DataContext = cargo };
                var addWindow = new AddWindow(editor);
                addWindow.ShowDialog();
            }
        }

        private void BtnEditUser_Click(object sender, RoutedEventArgs e)
        {
            if (lbUsers.SelectedItem != null)
            {
                var passenger = lbUsers.SelectedItem as Passenger;
                var editor = new AddPassenger() { DataContext = passenger };
                var addWindow = new AddWindow(editor);
                addWindow.ShowDialog();
            }
        }

        private void BtnAddVehicle_Click(object sender, RoutedEventArgs e)
        {
            var addVehicleControl = new AddVehicle(new List<Passenger>(objectsManager.Passengers),
                new List<Cargo>(objectsManager.Cargos), new List<Engine>(objectsManager.Engines));
            var addWindow = new AddWindow(addVehicleControl);
            if (addWindow.ShowDialog().Value)
            {
                var vehicle = addVehicleControl.Vehicle;
                foreach (IFunctionPlugin plugin in plugins)
                {
                    plugin.Execute(vehicle);
                }
                objectsManager.Vehicles.Add(vehicle);
            }
        }

        private void BtnAddEngine_Click(object sender, RoutedEventArgs e)
        {
            var addEngineControl = new AddEngine(new List<EnergySource>(objectsManager.EnergySources));
            var addWindow = new AddWindow(addEngineControl);
            if (addWindow.ShowDialog().Value)
            {
                objectsManager.Engines.Add(addEngineControl.Engine);
            }
        }

        private void BtnAddSource_Click(object sender, RoutedEventArgs e)
        {
            var source = new EnergySource();
            var addSourceControl = new AddSource() { DataContext = source };
            var addWindow = new AddWindow(addSourceControl);
            if (addWindow.ShowDialog().Value)
            {
                objectsManager.EnergySources.Add(source);
            }
        }

        private void BtnAddCargo_Click(object sender, RoutedEventArgs e)
        {
            var cargo = new Cargo();
            var addCargoControl = new AddCargo() { DataContext = cargo };
            var addWindow = new AddWindow(addCargoControl);
            if (addWindow.ShowDialog().Value)
            {
                objectsManager.Cargos.Add(cargo);
            }
        }

        private void BtnAddUser_Click(object sender, RoutedEventArgs e)
        {
            var passenger = new Passenger();
            var addPassengerControl = new AddPassenger() { DataContext = passenger };
            var addWindow = new AddWindow(addPassengerControl);
            if (addWindow.ShowDialog().Value)
            {
                objectsManager.Passengers.Add(passenger);
            }
        }

        private void btnSettings_Click(object sender, RoutedEventArgs e)
        {
            var settingsControl = new SettingsControl(cipheringPlugins);
            var view = new AddWindow(settingsControl);
            view.ShowDialog();
        }
    }
}
