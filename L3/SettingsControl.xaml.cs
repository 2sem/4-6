﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Classes.Plugins;

namespace L3
{
    /// <summary>
    /// Логика взаимодействия для SettingsControl.xaml
    /// </summary>
    public partial class SettingsControl : UserControl
    {
        public ObservableCollection<ICipheringPlugin> pluginsCollection;

        public SettingsControl(List<ICipheringPlugin> plugins)
        {
            InitializeComponent();
            pluginsCollection = new ObservableCollection<ICipheringPlugin>();
            lvPlugins.ItemsSource = pluginsCollection;

            foreach (ICipheringPlugin plugin in plugins)
            {
                pluginsCollection.Add(plugin);
            }

        }

    }
}
