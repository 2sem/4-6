﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Classes;

namespace L3
{
    public class ObjectsManager
    {
        public ObservableCollection<Passenger> Passengers { get; private set; } = new ObservableCollection<Passenger>();
        public ObservableCollection<Cargo> Cargos { get; private set; } = new ObservableCollection<Cargo>();
        public ObservableCollection<EnergySource> EnergySources { get; private set; } = new ObservableCollection<EnergySource>();
        public ObservableCollection<Engine> Engines { get; private set; } = new ObservableCollection<Engine>();
        public ObservableCollection<Vehicle> Vehicles { get; private set; } = new ObservableCollection<Vehicle>();

        private static volatile ObjectsManager instance;
        private static readonly object lockObject = new object();

        public static ObjectsManager Instance()
        {
            if (instance == null)
            {
                lock (lockObject)
                {
                    if (instance == null)
                    {
                        instance = new ObjectsManager();
                    }
                }
            }
            return instance;
        }
    }
}
