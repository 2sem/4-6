﻿using Classes;
using Classes.Plugins;

namespace Plugins
{
    public class CarPassengersPlugin : IFunctionPlugin
    {
        public void Execute(Vehicle vehicle)
        {
            var car = vehicle as Car;
            if (car != null)
            {
                car.PassengerCapacity = 4;
                car.Passengers.Clear();
            }
        }
    }
}
