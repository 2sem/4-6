﻿using Classes;
using Classes.Plugins;

namespace Plugins
{
    public class VehicleDrivingPlugin : IFunctionPlugin
    {
        public void Execute(Vehicle vehicle)
        {
            vehicle.Drive();
        }
    }
}
