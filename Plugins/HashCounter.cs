﻿using System.Security.Cryptography;
using System.Text;

namespace Plugins
{
    public class HashCounter : IAnotherPlugin
    {
        public string Execute(byte[] data)
        {
            using (SHA256 sha256Hash = SHA256.Create())
            {
                byte[] hash = sha256Hash.ComputeHash(data);

                var stringBuilder = new StringBuilder();

                for (int i = 0; i < data.Length; i++)
                {
                    stringBuilder.Append($"{data[i].ToString("x2")}");
                }
                return stringBuilder.ToString();
            }
        }
    }
}
