﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Classes;
using Classes.Plugins;

namespace Plugins
{
    class MessageShowingPlugin : IFunctionPlugin
    {
        public void Execute(Vehicle vehicle)
        {
            MessageBox.Show($"New vehicle {vehicle} was added");
        }
    }
}
