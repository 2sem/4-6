﻿using System.IO;
using System.Windows;
using Classes;
using Classes.Plugins;

namespace Plugins
{
    class HashPlugin : IFunctionPlugin
    {
        IAnotherPlugin anotherHashPlugin;

        public HashPlugin()
        {
            anotherHashPlugin = new HashCounter();
        }

        public void Execute(Vehicle vehicle)
        {
            string hash = "";
            using (var memoryStream = new MemoryStream())
            {
                using (var streamWriter = new StreamWriter(memoryStream))
                {
                    streamWriter.Write(vehicle.GetType().ToString());
                }
                hash = anotherHashPlugin.Execute(memoryStream.ToArray());
            }
            MessageBox.Show(hash);
        }
    }
}
