﻿using Classes.Plugins;

namespace Plugins
{
    class CaesarCiphering : ICipheringPlugin
    {
        public int Key { get; set; } = 10;

        public string Name => "Caesar";

        public bool Enabled { get; set; }

        private int n = 256;

        public void Decipher(byte[] array, int offset, int count)
        {
            for (int i = 0; i < count; i++)
            {
                array[offset + i] = (byte)((array[offset + i] + n - Key) % n);
            }
        }

        public void Encipher(byte[] array, int offset, int count)
        {
            for (int i = 0; i < count; i++)
            {
                array[offset + i] = (byte)((array[offset + i] + Key) % n);
            }
        }
    }
}
