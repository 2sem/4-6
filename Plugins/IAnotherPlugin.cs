﻿namespace Plugins
{
    interface IAnotherPlugin
    {
        string Execute(byte[] data);
    }
}
