﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;
using System.Xml.Serialization;

namespace Classes
{

    [XmlInclude(typeof(Truck)), XmlInclude(typeof(Car))]
    public abstract class Vehicle : INotifyPropertyChanged
    {
        private Engine engine;

        public event PropertyChangedEventHandler PropertyChanged;

        [Display(Name = "Двигатель", GroupName = "ComboBox")]
        public Engine Engine { get => engine; set { engine = value; OnPropertyChanged(); } }
        public void Drive()
        {
            Engine?.Start();
        }
        public Vehicle() { }
        public Vehicle(Engine engine)
        {
            Engine = engine;
        }
        public override string ToString()
        {
            return Engine?.ToString();
        }

        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
