﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;
using System.Xml.Serialization;


namespace Classes
{
    [XmlInclude(typeof(DieselEngine)), XmlInclude(typeof(ElectricEngine))]
    [DisplayColumn(displayColumn: "Тип двигателя")]
    public abstract class Engine : INotifyPropertyChanged
    {
        private EnergySource energySource;

        [Display(Name = "Топливохранилище", GroupName = "ComboBox")]
        public EnergySource EnergySource { get => energySource; set { energySource = value; OnPropertyChanged(); } }

        public event PropertyChangedEventHandler PropertyChanged;

        public abstract void Start();
        public abstract void Stop();
        public abstract void Accelerate();
        public Engine(EnergySource energySource)
        {
            EnergySource = energySource;
        }
        public Engine() { }
        public string String { get => ToString(); }
        public override string ToString()
        {
            return EnergySource?.ToString();
        }

        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}