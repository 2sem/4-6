﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Classes
{
    public class EnergySource : INotifyPropertyChanged
    {
        private int capacity;
        private int currentLevel;
        public int Capacity { get => capacity; set { capacity = value; OnPropertyChanged(); } }
        public int CurrentLevel { get => currentLevel; set { currentLevel = value; OnPropertyChanged(); } }

        public event PropertyChangedEventHandler PropertyChanged;

        public void Add(int amount)
        {
            CurrentLevel += amount;
        }
        public void Remove(int amount)
        {
            CurrentLevel -= amount;
        }
        public void Refuel()
        {
            CurrentLevel = Capacity;
        }
        public EnergySource(int capacity)
        {
            Capacity = capacity;
        }
        public EnergySource() { }
        public override string ToString()
        {
            return this.GetType().Name + " " + Capacity + " " + CurrentLevel;
        }

        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
