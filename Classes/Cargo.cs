﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Classes
{
    public class Cargo : INotifyPropertyChanged
    {
        private int weight;
        private string name;

        public int Weight { get => weight; set { weight = value; OnPropertyChanged(); } }
        public string Name { get => name; set { name = value; OnPropertyChanged(); } }

        public event PropertyChangedEventHandler PropertyChanged;

        public override string ToString()
        {
            return Name + " " + Weight;
        }

        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
