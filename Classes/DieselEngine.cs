﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;

namespace Classes
{
    public class DieselEngine : Engine, INotifyPropertyChanged
    {
        private int fuelConsumption;
        private int currentVolume;
        private int volume;

        [Display(Name = "Потребление топлива", GroupName = "TextBox")]
        public int FuelConsumption { get => fuelConsumption; set { fuelConsumption = value; OnPropertyChanged(); } }
        [Display(Name = "Текущий уровень", GroupName = "TextBox")]
        public int CurrentVolume { get => currentVolume; set { currentVolume = value; OnPropertyChanged(); } }
        [Display(Name = "Объём", GroupName = "TextBox")]
        public int Volume { get => volume; set { volume = value; OnPropertyChanged(); } }
        private const int InitialFuelConsumption = 1;

        public DieselEngine(EnergySource energySource, int volume) : base(energySource)
        {
            Volume = volume;
        }
        public DieselEngine() { }

        public override void Start()
        {
            FuelConsumption = InitialFuelConsumption;
            while (FuelConsumption != 0 && EnergySource.CurrentLevel >= FuelConsumption)
            {
                Inlet();
                Compress();
                Expand();
                Release();
            }
        }
        private void Inlet()
        {
            CurrentVolume++;
        }
        private void Compress()
        {
            CurrentVolume--;
        }
        private void Expand()
        {
            EnergySource.Remove(FuelConsumption);
            CurrentVolume += FuelConsumption;
        }
        private void Release()
        {
            CurrentVolume--;
        }

        public override void Stop()
        {
            FuelConsumption = 0;
        }

        public override void Accelerate()
        {
            FuelConsumption++;
        }

        public override string ToString()
        {
            return this.GetType().Name + " " + FuelConsumption + " " + CurrentVolume
                + " " + Volume + " " + base.ToString();
        }

    }
}
