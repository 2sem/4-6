﻿namespace Classes.Plugins
{
    public interface IFunctionPlugin : IPlugin
    {
        void Execute(Vehicle vehicle);
    }
}
