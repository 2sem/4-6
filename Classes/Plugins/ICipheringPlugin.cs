﻿namespace Classes.Plugins
{
    public interface ICipheringPlugin
    {
        string Name { get; }
        bool Enabled { get; set; }
        void Encipher(byte[] array, int offset, int count);
        void Decipher(byte[] array, int offset, int count);
    }
}
