﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Classes.Plugins
{
    public class PluginLoader
    {
        private const string LibraryPattern = "*.dll";

        public static List<T> LoadPlugins<T>(string path)
        {
            string[] paths = Directory.GetFiles(path, LibraryPattern);
            List<T> plugins = new List<T>();


            foreach (string libraryPath in paths)
            {
                Assembly assembly = Assembly.LoadFrom(libraryPath);

                if (IsSigned(assembly) && IsValidSignature(assembly))
                {
                    Type[] pluginTypes = null;
                    try
                    {

                        pluginTypes = assembly.GetTypes();
                        foreach (Type type in pluginTypes)
                        {
                            if (type.GetInterface(typeof(T).Name) != null)
                            {
                                var plugin = (T)Activator.CreateInstance(type);
                                plugins.Add(plugin);
                            }
                        }
                    }
                    catch (ReflectionTypeLoadException e)
                    {

                    }
                }
            }
            return plugins;
        }

        private static bool IsSigned(Assembly assembly)
        {
            return assembly.GetName().GetPublicKey().Length > 0;
        }

        private static bool IsValidSignature(Assembly assembly)
        {
            byte[] validKey = Assembly.GetExecutingAssembly().GetName().GetPublicKeyToken();
            byte[] assemblyKey = assembly.GetName().GetPublicKeyToken();

            var result = validKey.Length == assemblyKey.Length;

            if (result)
            {
                for (int i = 0; i < validKey.Length; i++)
                {
                    result &= validKey[i] == assemblyKey[i];
                }
            }

            return result;
        }
    }
}
