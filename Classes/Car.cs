﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;

namespace Classes
{
    public class Car : Vehicle, INotifyPropertyChanged
    {
        private List<Passenger> passengers;
        private int passengerCapacity;

        [Display(Name = "Пассажиры", GroupName = "ListBox", ResourceType = typeof(Passenger))]
        public List<Passenger> Passengers
        {
            get => passengers; set
            {
                passengers = value; OnPropertyChanged(); OnPropertyChanged(nameof(PassengersCount));
            }
        }
        public int PassengersCount { get => Passengers?.Count ?? 0; }
        [Display(Name = "Число мест", GroupName = "TextBox")]
        public int PassengerCapacity { get => passengerCapacity; set { passengerCapacity = value; OnPropertyChanged(); } }
        public Car(Engine engine, int passengerCapacity) : base(engine)
        {
            Passengers = new List<Passenger>(passengerCapacity);
        }
        public Car() { Passengers = new List<Passenger>(); }
        public bool AddPassenger(Passenger passenger)
        {
            passenger.PropertyChanged += OnPassengerChanged;
            if (PassengersCount < PassengerCapacity)
            {
                Passengers.Add(passenger);
                OnPropertyChanged(nameof(Passengers));
                return true;
            }
            return false;
        }

        private void OnPassengerChanged(object sender, PropertyChangedEventArgs e)
        {
            OnPropertyChanged(nameof(Passengers));
        }

        public bool RemovePassenger(Passenger passenger)
        {
            bool deleted = Passengers.Remove(passenger);
            if (deleted)
            {
                passenger.PropertyChanged -= OnPassengerChanged;
            }
            OnPropertyChanged(nameof(Passengers));
            return deleted;
        }
        public override string ToString()
        {
            return GetType().Name + " " + base.ToString() + " " + PassengerCapacity
                + " " + PassengersCount;
        }
    }
}
