﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Classes
{
    public class Truck : Vehicle
    {
        private List<Cargo> cargos;
        private int currentCapacity;
        private int maxCarryingCapacity;

        [Display(Name = "Грузы", GroupName = "ListBox", ResourceType = typeof(Cargo))]
        public List<Cargo> Cargos { get => cargos; set { cargos = value; OnPropertyChanged(); } }
        public int CurrentCapacity { get => currentCapacity; set { currentCapacity = value; OnPropertyChanged(); } }
        [Display(Name = "Грузоподъёмность", GroupName = "TextBox")]
        public int MaxCarryingCapacity { get => maxCarryingCapacity; set { maxCarryingCapacity = value; OnPropertyChanged(); } }
        public Truck(Engine engine, int carryingCapacity) : base(engine)
        {
            MaxCarryingCapacity = carryingCapacity;
            Cargos = new List<Cargo>();
        }
        public Truck() { Cargos = new List<Cargo>(); }
        public bool AddCargo(Cargo cargo)
        {
            bool canAdd = cargo.Weight <= MaxCarryingCapacity - CurrentCapacity;
            if (canAdd)
            {
                cargo.PropertyChanged += OnCargoChanged;
                CurrentCapacity += cargo.Weight;
                Cargos.Add(cargo);
                OnPropertyChanged(nameof(Cargos));
            }
            return canAdd;
        }

        private void OnCargoChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            OnPropertyChanged(nameof(Cargos));
        }

        public bool RemoveCargo(Cargo cargo)
        {
            bool deleted = Cargos.Remove(cargo);
            if (deleted)
            {
                cargo.PropertyChanged -= OnCargoChanged;
                CurrentCapacity -= cargo.Weight;
                OnPropertyChanged(nameof(Cargos));
            }
            return deleted;
        }
        public override string ToString()
        {
            return GetType().Name + " " + MaxCarryingCapacity + " " + CurrentCapacity
                + " " + base.ToString();
        }
    }
}
